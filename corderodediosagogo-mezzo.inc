\context Staff = "mezzosoprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzosoprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key f \major

		R2*5  |
		r4 r8 c'  |
		f' 8 f' f' 4  |
		d' 4. d' 8  |
		e' 8 e' e' e'  |
%% 10
		e' 8 d' e' c' ~  |
		c' 8 c' c' c'  |
		c' 4 c' 8 c'  |
		c' 4 c' 8 c'  |
		c' 8 bes a a ~  |
%% 15
		a 8 a 4. ~  |
		a 2 ~  |
		a 2 ~  |
		a 8 r r c'  |
		f' 8 f' f' 4  |
%% 20
		d' 4. d' 8  |
		e' 8 e' e' e'  |
		e' 8 d' e' c' ~  |
		c' 8 c' c' c'  |
		c' 4 c' 8 c'  |
%% 25
		c' 4 c' 8 c'  |
		c' 8 bes a a ~  |
		a 8 a 4. ~  |
		a 2 ~  |
		a 2 ~  |
%% 30
		a 8 r r c'  |
		f' 8 f' f' 4  |
		d' 4. d' 8  |
		e' 8 e' e' e'  |
		e' 8 d' e' c' ~  |
%% 35
		c' 8 c' c' 4  |
		c' 4 c'  |
		c' 2 ~  |
		c' 4 r  |
		c' 2  |
%% 40
		bes 4 c'  |
		a 2 ~  |
		a 2  |
		R2*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzosoprano" {
		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do,
		ten pie -- dad, ten pie -- dad, ten pie -- dad de no -- so -- tros. __

		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do,
		ten pie -- dad, ten pie -- dad, ten pie -- dad de no -- so -- tros. __

		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do,
		da -- nos la paz, __
		da -- nos la paz. __
	}
>>

