%\context ChordNames \chords {
%	\set chordNameFunction = #parenthesis-ignatzek-chord-names
%	\set instrumentName = #"(Capo 3)"
%	\set chordChanges = ##t
%	\set majorSevenSymbol = \markup { "maj7" }
%	\transpose f d { \acordes }
%}

\context ChordNames \chords {
	\set chordChanges = ##t
	\set majorSevenSymbol = \markup { "maj7" }
	\acordes
}
