\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key f \major

		R2*5  |
		r4 r8 c  |
		f 8 f f 4  |
		d 4. d 8  |
		e 8 e e e  |
%% 10
		e 8 d e f ~  |
		f 8 f r4  |
		r4 a 8 a  |
		a 4 c' 8 c'  |
		c' 8 bes a a ~  |
%% 15
		a 8 a 4. ~  |
		a 2 ~  |
		a 2 ~  |
		a 8 r r c  |
		f 8 f f 4  |
%% 20
		d 4. d 8  |
		e 8 e e e  |
		e 8 d e f ~  |
		f 8 f r4  |
		r4 a 8 a  |
%% 25
		a 4 c' 8 c'  |
		c' 8 bes a a ~  |
		a 8 a 4. ~  |
		a 2 ~  |
		a 2 ~  |
%% 30
		a 8 r r c  |
		f 8 f f 4  |
		d 4. d 8  |
		e 8 e e e  |
		e 8 d e f ~  |
%% 35
		f 8 f a 4  |
		a 4 a  |
		f 2 ~  |
		f 4 r  |
		bes 2  |
%% 40
		g 4 bes  |
		a 2 ~  |
		a 2  |
		R2*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do,
		ten pie -- dad, ten pie -- dad de no -- so -- tros. __

		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do,
		ten pie -- dad, ten pie -- dad de no -- so -- tros. __

		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do,
		da -- nos la paz, __
		da -- nos la paz. __
	}
>>

