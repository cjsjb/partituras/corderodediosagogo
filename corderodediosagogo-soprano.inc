\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key f \major

		R2*5  |
		r4. c' 8  |
		f' 8 f' f' 4  |
		d' 4. d' 8  |
		e' 8 e' e' e'  |
%% 10
		e' 8 d' e' f' ~  |
		f' 8 f' a' a'  |
		a' 4 r  |
		r4 a' 8 a'  |
		a' 8 g' f' f' ~  |
%% 15
		f' 8 f' 4. ~  |
		f' 2 ~  |
		f' 2 ~  |
		f' 8 r r c'  |
		f' 8 f' f' 4  |
%% 20
		d' 4. d' 8  |
		e' 8 e' e' e'  |
		e' 8 d' e' f' ~  |
		f' 8 f' a' a'  |
		a' 4 r  |
%% 25
		r4 a' 8 a'  |
		a' 8 g' f' f' ~  |
		f' 8 f' 4. ~  |
		f' 2 ~  |
		f' 2 ~  |
%% 30
		f' 8 r r c'  |
		f' 8 f' f' 4  |
		d' 4. d' 8  |
		e' 8 e' e' e'  |
		e' 8 d' e' f' ~  |
%% 35
		f' 8 f' a' 4  |
		a' 4 a'  |
		f' 2 ~  |
		f' 4 r  |
		f' 2  |
%% 40
		d' 4 e'  |
		f' 2 ~  |
		f' 2  |
		R2*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do,
		ten pie -- dad, ten pie -- dad de no -- so -- tros. __

		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do,
		ten pie -- dad, ten pie -- dad de no -- so -- tros. __

		Cor -- de -- ro de Dios,
		que qui -- tas el pe -- ca -- do del mun -- do,
		da -- nos la paz, __
		da -- nos la paz. __
	}
>>

